import random as rd

seance = 5
row, col = 15, 12

array = [[0 for x in range(col)] for y in range(row)]
info = {}

for n in range(seance):
    info[n] = list(map(list, array))


def check_val(val, length, val_type, a, b):
    err = True
    val = trn(val)
    if len(val) == length:
        for i in range(length):
            if type(val[i]) == val_type and a[i] < val[i] <= b[i]:
                err = False
                continue
            else:
                err = True
                break

    if not err:
        return True
    else:
        return False


def trn(val, val_type=int):
    for i in range(len(val)):
        val[i] = val_type(val[i])
    return val


def set_n(val):
    val = int(val) - 1
    return val


def get_input():
    command = input(": ").split()
    com_word = command[0]

    if com_word in special_command and len(command) == 1:
        sp = special_command[com_word]
        return sp()
    elif com_word in special_command and len(command) > 1:
        print("Error. To long Command")
    else:
        if com_word in com_dict:
            com = com_dict[com_word]

            if len(command) >= 2:
                noun_word = command[1:]
                return com(noun_word)
            else:
                print("Error. To short Command")
        else:
            print('Unknown Command "{}"'.format(com_word))


def exit_from_code():
    return exit()


def random():
    for s in range(seance):
        for i in range(row):
            for j in range(col):
                info[s][i][j] = round(rd.random())
    return print('You randomized Data')


def swipe(val):
    pattern = ['□', '■']
    out = list(map(list, val))
    for i in range(row):
        for j in range(col):
            if val[i][j] == 0:
                out[i][j] = pattern[0]
            elif val[i][j] == 1:
                out[i][j] = pattern[1]
    return out


def show(val):
    val = trn(val)
    if check_val(val, 1, int, [0], [5]):
        out_data = swipe(info[val[0] - 1])
        for i in range(row):
            print(' '.join(out_data[i]))
    else:
        print("Error. Value is not Integer or Out of range")


def add(val):
    val = trn(val)
    s = set_n(val[0])
    r = set_n(val[1])
    c = set_n(val[2])

    if check_val(val, 3, int, [0, 0, 0], [5, row, col]):
        if info[s][r][c] != 1:
            info[s][r][c] = 1
            print("You are reserved the seat")
        else:
            print("Error. The seat is already reserved")
    else:
        print("Wrong Value")


def delete(val):
    val = trn(val)
    s = set_n(val[0])
    r = set_n(val[1])
    c = set_n(val[2])

    if check_val(val, 3, int, [0, 0, 0], [5, row, col]):
        if info[s][r][c] == 1:
            info[s][r][c] = 0
            print("You are unreserved the seat")
        else:
            print("Error. The seat is already unreserved")
    else:
        print("Wrong Value")


special_command = {
    'exit': exit_from_code,
    'random': random
}

com_dict = {
    'show': show,
    'add': add,
    'del': delete
}

while True:
    get_input()
